import pygame
import random
import math
import sys, os

def resource_path(relative_path):
    #needed for PyInstaller
    #use command: py -m PyInstaller --onefile --noconsole game.py --hidden-import game
    try:
        base_path = sys._MEIPASS
    except Exception:
        base_path = os.path.abspath(".")

    return os.path.join(base_path, relative_path)


SCREEN_HEIGHT = 700
SCREEN_WIDTH = 1.3 * SCREEN_HEIGHT
TIME_DELAY = 10
run = True

RECT_SIZE = 150

MAX_DISTANCE = math.sqrt(3* (255**2))

# init game + info
pygame.init()
pygame.display.set_caption("ColorFind")

surface = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))

font = pygame.font.SysFont('Consolas', 30)
target_color = (random.choice(range(0, 256)), random.choice(range(0, 256)), random.choice(range(0, 256)))

background = pygame.Rect(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)

red = (255/2, 0, 0)
green = (0, 255/2, 0)
blue = (0, 0, 255/2)

slider_height = SCREEN_HEIGHT/2.5 + RECT_SIZE + 10

red_color = pygame.Rect(100 - RECT_SIZE/2, SCREEN_HEIGHT/2.5 , RECT_SIZE, RECT_SIZE)
red_slider_draging = False
red_slider_edges = (100 - RECT_SIZE/2, 100-RECT_SIZE/2 + RECT_SIZE)
red_slider = pygame.Rect(red_slider_edges[1] - RECT_SIZE/2 - 5, slider_height, 10, 10)

green_color = pygame.Rect(SCREEN_WIDTH/2 - RECT_SIZE/2 , SCREEN_HEIGHT/2.5 , RECT_SIZE, RECT_SIZE)
green_slider_dragging = False
green_slider_edges = (SCREEN_WIDTH/2 - RECT_SIZE/2, SCREEN_WIDTH/2 - RECT_SIZE/2 + RECT_SIZE)
green_slider = pygame.Rect(green_slider_edges[1] - RECT_SIZE/2 - 5, slider_height, 10, 10)

blue_color = pygame.Rect(SCREEN_WIDTH-100 - RECT_SIZE/2, SCREEN_HEIGHT/2.5 , RECT_SIZE, RECT_SIZE)
blue_slider_dragging = False
blue_slider_edges = (SCREEN_WIDTH-100 - RECT_SIZE/2, SCREEN_WIDTH-100 - RECT_SIZE/2 + RECT_SIZE)
blue_slider = pygame.Rect(blue_slider_edges[1] - RECT_SIZE/2 - 5, slider_height, 10, 10)

start_ticks=pygame.time.get_ticks()
picking = True

while run:
    pygame.time.delay(TIME_DELAY)

    pygame.draw.rect(surface, (0,0,0), background)
    pygame.draw.rect(surface, target_color, pygame.Rect(SCREEN_WIDTH/2 - RECT_SIZE/2, 10 , RECT_SIZE, RECT_SIZE))

    pygame.draw.rect(surface, (red[0],green[1],blue[2]), pygame.Rect(SCREEN_WIDTH/2 - RECT_SIZE/2, SCREEN_HEIGHT - RECT_SIZE - 10 , RECT_SIZE, RECT_SIZE))

    if(picking):

        seconds= round(( 20 - (pygame.time.get_ticks()-start_ticks)/1000 ), 2)
        if(seconds < 0):
            picking = False

        #timer 
        surface.blit(font.render(str(seconds), True, (255, 255, 255)), (32, 48))

      
        pygame.draw.rect(surface, red, red_color)
        pygame.draw.rect(surface, green, green_color)
        pygame.draw.rect(surface, blue, blue_color)

        #sliders
        pygame.draw.rect(surface, (100,100,100), pygame.Rect(red_slider_edges[0],  slider_height, RECT_SIZE, 10))
        pygame.draw.rect(surface, (255,255,255), red_slider)

        pygame.draw.rect(surface, (100,100,100), pygame.Rect(green_slider_edges[0],  slider_height, RECT_SIZE, 10))
        pygame.draw.rect(surface, (255,255,255), green_slider)

        pygame.draw.rect(surface, (100,100,100), pygame.Rect(blue_slider_edges[0],  slider_height, RECT_SIZE, 10))
        pygame.draw.rect(surface, (255,255,255), blue_slider)


    else:
        surface.blit(font.render("results", True, (255, 255, 255)), (SCREEN_WIDTH/4 - RECT_SIZE/2 , SCREEN_HEIGHT/2.5))
        text = "target: " + str(target_color)
        surface.blit(font.render(text, True, (255, 255, 255)), (SCREEN_WIDTH/4 - RECT_SIZE/2 , SCREEN_HEIGHT/2.5 + 30))
        text = "guess: " + str((int(red[0]),int(green[1]),int(blue[2])))
        surface.blit(font.render(text, True, (255, 255, 255)), (SCREEN_WIDTH/4 - RECT_SIZE/2 , SCREEN_HEIGHT/2.5 + 60))

        distance = math.sqrt((target_color[0] - int(red[0]))**2 + (target_color[1] - int(green[1]))**2 + (target_color[2] - int(blue[2]))**2)
        score = 100 - round((distance/MAX_DISTANCE)*100,2)

        text = "score: " + str(score) + "%"
        surface.blit(font.render(text, True, (255, 255, 255)), (SCREEN_WIDTH/4 - RECT_SIZE/2 , SCREEN_HEIGHT/2.5 + 90))
        surface.blit(font.render("press ENTER to try again", True, (255, 255, 255)), (SCREEN_WIDTH/4 - RECT_SIZE/2 , SCREEN_HEIGHT/2.5 + 120))
        

    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            run = False

        elif event.type == pygame.MOUSEBUTTONDOWN:
            if event.button == 1:            
                if red_slider.collidepoint(event.pos):
                    red_slider_draging = True
                    mouse_x, _ = event.pos
                    offset_x = red_slider.x - mouse_x

                elif green_slider.collidepoint(event.pos):
                    green_slider_dragging = True
                    mouse_x, _ = event.pos
                    offset_x = green_slider.x - mouse_x

                elif blue_slider.collidepoint(event.pos):
                    blue_slider_dragging = True
                    mouse_x, _ = event.pos
                    offset_x = blue_slider.x - mouse_x


        elif event.type == pygame.MOUSEMOTION and picking:
            if red_slider_draging:
                mouse_x, _ = event.pos
                red_slider.x = max(red_slider_edges[0], min(mouse_x + offset_x, red_slider_edges[1] - 10))
                red = ((red_slider.x - red_slider_edges[0])/(RECT_SIZE-10) * 255, 0, 0)

            elif green_slider_dragging:
                mouse_x, _ = event.pos
                green_slider.x = max(green_slider_edges[0], min(mouse_x + offset_x, green_slider_edges[1] - 10))
                green = (0, (green_slider.x - green_slider_edges[0])/(RECT_SIZE-10) * 255, 0)

            elif blue_slider_dragging:
                mouse_x, _ = event.pos
                blue_slider.x = max(blue_slider_edges[0], min(mouse_x + offset_x, blue_slider_edges[1] - 10))
                blue = (0, 0, (blue_slider.x - blue_slider_edges[0])/(RECT_SIZE-10) * 255)

        elif event.type == pygame.MOUSEBUTTONUP:
            if event.button == 1:            
                red_slider_draging = False
                green_slider_dragging = False
                blue_slider_dragging = False

        
    if event.type == pygame.KEYDOWN:
        if event.key == pygame.K_RETURN and not picking: 
            target_color = (random.choice(range(0, 256)), random.choice(range(0, 256)), random.choice(range(0, 256)))
            start_ticks=pygame.time.get_ticks()
            picking = True


        
    pygame.display.update() 