# Color finder game

![Screenshots of gameplay and scoreboard](image.png)*gameplay and scoreboard*

## info

Simple game made with pygame. 

The goal is to replicate target color most accurately by moving the three sliders: Red, Green and Blue. Time limit is 20 seconds. 

## score

Color values are stored in RGB format: 3 numbers in range [0, 255] indicating "how much" of red/green/blue is there.

Euclidean distance is used for calculating the final score:

```math

distance = \sqrt{(R_{result} - R_{guess})^2 + (G_{result} - G_{guess})^2 + (B_{result} - B_{guess})^2}
```

Percentage is calculated by dividng the distance by maximum distance $` = \sqrt{3 \times (255)^2} `$

## future work

I might add another mode: blind mode.

Unlimited time, but player cannot see the result color of their individual RGB sliding until after they had submitted their guess.



## executable

You can create .exe file using PyInstaller. 

Something like `py -m PyInstaller --onefile --noconsole game.py --hidden-import game` will do.
